package com.wever.android.marsweather.ui.weather;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.models.WeatherReportResponse;
import com.wever.android.marsweather.ui.base.BaseFragment;
import com.wever.android.marsweather.util.DaggerApplication;
import com.wever.android.marsweather.util.UserPreferences;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by brandon on 1/16/16.
 */
public class MarsReportFragment extends BaseFragment
    implements SwipeRefreshLayout.OnRefreshListener {

  public static String TAG = "MARS_REPORT_FRAGMENT";

  @Bind(R.id.root) LinearLayout rootView;
  @Bind(R.id.recyclerview) RecyclerView reportRecycler;
  @Bind(R.id.refresh_layout) SwipeRefreshLayout refreshLayout;

  private int temperatureUnit = UserPreferences.getTemperatureUnit();

  MarsReportAdapter reportAdapter;

  public static MarsReportFragment newInstance() {
    Bundle args = new Bundle();
    MarsReportFragment fragment = new MarsReportFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    reportAdapter = new MarsReportAdapter();
    pullReports();
  }

  @Override public void onResume() {
    super.onResume();
    if (temperatureUnit != UserPreferences.getTemperatureUnit()) {
      temperatureUnit = UserPreferences.getTemperatureUnit();
      reportAdapter.notifyDataSetChanged();
    }
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.refreshable_recycler, container, false);
    ButterKnife.bind(this, root);
    reportRecycler.setAdapter(reportAdapter);
    reportRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
    refreshLayout.setOnRefreshListener(this);
    return root;
  }


  private void pullReports() {
    if (DaggerApplication.isConnectedToInternet()) {
      restClient.getMarsWeatherReportService()
          .getWeatherReports(1)
          .enqueue(new Callback<WeatherReportResponse>() {
            @Override
            public void onResponse(Response<WeatherReportResponse> response, Retrofit retrofit) {
              if (response.isSuccess() && response.body() != null) {
                if (response.body().getResults() != null) {
                  reportAdapter.addReports(response.body().getResults());
                }
              }

              refreshLayout.setRefreshing(false);
            }

            @Override public void onFailure(Throwable t) {
              refreshLayout.setRefreshing(false);
            }
          });
    } else {
      presentNoNetworkNotice();
    }
  }

  @Override public void onRefresh() {
    reportAdapter = new MarsReportAdapter();
    reportRecycler.swapAdapter(reportAdapter, true);
    pullReports();
  }

  // TODO change this to a snackbar, alert is too much for this.
  private void presentNoNetworkNotice(){
    refreshLayout.setRefreshing(false);
    new MaterialDialog.Builder(getContext()).title("No Network Available").negativeText("Cancel").positiveText("Settings").onPositive(
        new MaterialDialog.SingleButtonCallback() {
          @Override public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
          }
        }).show();
  }

}

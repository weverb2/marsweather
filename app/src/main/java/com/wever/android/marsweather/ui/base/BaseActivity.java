package com.wever.android.marsweather.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.squareup.otto.Bus;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.util.DaggerApplication;
import javax.inject.Inject;

/**
 * Created by brandon on 10/16/15.
 */
public class BaseActivity extends AppCompatActivity {

  @Inject Bus applicationBus;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DaggerApplication.getComponent(this).inject(this);
  }

  @Override protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override protected void onResume() {
    super.onResume();
    applicationBus.register(this);
  }

  @Override protected void onPause() {
    applicationBus.unregister(this);
    super.onPause();
  }

  @Override public void startActivity(Intent intent) {
    super.startActivity(intent);
    this.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
  }

  @Override public void onBackPressed() {
    this.overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
    super.onBackPressed();
  }
}

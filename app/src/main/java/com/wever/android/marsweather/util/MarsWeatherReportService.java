package com.wever.android.marsweather.util;

import com.wever.android.marsweather.models.WeatherReportResponse;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by brandon on 1/16/16.
 */
public interface MarsWeatherReportService {
  @GET("v1/archive/") Call<WeatherReportResponse> getWeatherReports(@Query("page") int pageNumber);
}

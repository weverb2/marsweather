package com.wever.android.marsweather.ui.settings;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.ui.base.ButterKnifeViewHolder;
import java.util.ArrayList;

/**
 * Created by brandon on 1/16/16.
 */
public class SettingsOptionsAdapter extends RecyclerView.Adapter<SettingsOptionsAdapter.SettingsRowViewHolder> {

  public static final int TEMPERATURE_VIEW_TYPE = 0;

  ArrayList<Pair<String, String>> models;

  public SettingsOptionsAdapter() {
    models = new ArrayList<>();
  }

  @Override public SettingsRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View root = inflater.inflate(R.layout.card_settings_row, parent, false);
    return new SettingsRowViewHolder(root);
  }

  @Override public void onBindViewHolder(SettingsRowViewHolder holder, int position) {
    holder.bindWithModel(models.get(position));
  }

  @Override public int getItemCount() {
    return models.size();
  }

  public void setModels(ArrayList<Pair<String, String>> models) {
    this.models = models;
    notifyDataSetChanged();
  }

  @Override public int getItemViewType(int position) {
    return position;
  }

  public void addModel(Pair<String, String> model){
    models.add(model);
    notifyItemInserted(models.size() - 1);
  }

  public void setModelAtIndex(Pair<String, String> model, int index){
    if (models.size() > index) {
      models.set(index, model);
      notifyItemChanged(index);
    } else {
      addModel(model);
    }
  }

  public static class SettingsRowViewHolder extends ButterKnifeViewHolder<Pair<String, String>>{

    @Bind(R.id.title) TextView title;
    @Bind(R.id.value) TextView value;

    public SettingsRowViewHolder(View itemView) {
      super(itemView);
    }

    @Override public void bindWithModel(@Nullable Pair<String, String> model) {
      if (model == null){
        itemView.setVisibility(View.GONE);
      } else {
        title.setText(model.first);
        value.setText(model.second);
      }
    }
  }
}

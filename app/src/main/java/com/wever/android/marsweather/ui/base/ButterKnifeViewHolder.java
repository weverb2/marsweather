package com.wever.android.marsweather.ui.base;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.ButterKnife;

/**
 * Created by brandon on 12/9/15.
 */
public abstract class ButterKnifeViewHolder<T> extends RecyclerView.ViewHolder {
  public ButterKnifeViewHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public abstract void bindWithModel(@Nullable T model);
}

package com.wever.android.marsweather.ui.weather;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.models.WeatherReport;
import com.wever.android.marsweather.ui.base.ButterKnifeViewHolder;
import com.wever.android.marsweather.util.DateUtils;
import com.wever.android.marsweather.util.UserPreferences;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

/**
 * Created by brandon on 1/16/16.
 */
public class MarsReportAdapter
    extends RecyclerView.Adapter<MarsReportAdapter.WeatherReportViewHolder> {

  ArrayList<WeatherReport> reports = new ArrayList<>();
  HashSet<Integer> selectedReports = new HashSet<>();
  private int lastPosition = -1;

  @Override public WeatherReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View root = inflater.inflate(R.layout.card_forecast_day, parent, false);
    return new WeatherReportViewHolder(root);
  }

  @Override public void onBindViewHolder(WeatherReportViewHolder holder, int position) {
    WeatherReport model = reports.get(position);
    holder.bindWithModel(model);
    holder.itemView.setTag(position);
    if (selectedReports.contains(position)) {
      holder.expandExtras();
    } else {
      holder.collapseExtras();
    }
    setAnimation(holder.itemView, position);
  }

  @Override public int getItemCount() {
    if (reports.size() > 7) {
      return 7;
    }
    return reports.size();
  }

  public void addReports(List<WeatherReport> newReports) {
    int size = reports.size();
    reports.addAll(newReports);
    notifyItemRangeInserted(size, newReports.size());
  }

  private void setAnimation(View viewToAnimate, int position) {
    // If the bound view wasn't previously displayed on screen, it's animated
    if (position > lastPosition) {
      Animation animation =
          AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
      viewToAnimate.startAnimation(animation);
      lastPosition = position;
    }
  }

  public class WeatherReportViewHolder extends ButterKnifeViewHolder<WeatherReport> {

    @Bind(R.id.card_background) CardView card;
    @Bind(R.id.sol_number) TextView solNumber;
    @Bind(R.id.atmospheric_opacity) TextView atmoOpacity;
    @Bind(R.id.earth_day) TextView earthDay;
    @Bind(R.id.high_temp_content) TextView highTemp;
    @Bind(R.id.low_temp_content) TextView lowTemp;
    @Bind(R.id.sunrise_date) TextView sunriseDate;
    @Bind(R.id.sunrise_time) TextView sunriseTime;
    @Bind(R.id.sunset_date) TextView sunsetDate;
    @Bind(R.id.sunset_time) TextView sunsetTime;
    @Bind(R.id.wind_direction) TextView windDirection;
    @Bind(R.id.wind_speed) TextView windSpeed;
    @Bind(R.id.pressure_string) TextView relativePressure;
    @Bind(R.id.pressure_unit) TextView pressurePascals;
    @Bind(R.id.forecast_extra) LinearLayout forecastExtra;

    public WeatherReportViewHolder(View itemView) {
      super(itemView);
    }

    public void expandExtras() {
      forecastExtra.setVisibility(View.VISIBLE);
    }

    public void collapseExtras() {
      forecastExtra.setVisibility(View.GONE);
    }

    @Override public void bindWithModel(@Nullable WeatherReport model) {
      if (model == null) {
        itemView.setVisibility(View.GONE);
      } else {

        // TODO come up with better solution, notifyItemChanged causes flashes and this smells bad.
        itemView.setOnClickListener(new View.OnClickListener() {
          @Override public void onClick(View v) {
            if (v.getTag() instanceof Integer) {
              int position = (int) v.getTag();
              if (selectedReports.contains(position)) {
                collapseExtras();
                selectedReports.remove(position);
              } else {
                expandExtras();
                selectedReports.add(position);
              }
            }
          }
        });

        Context context = itemView.getContext();
        solNumber.setText(String.valueOf(model.getSol()));
        atmoOpacity.setText(String.valueOf(model.getAtmoOpacity()));
        String earthDayString =
            context.getString(R.string.earth_day) + " " + model.getTerrestrialDate();
        earthDay.setText(earthDayString);

        windDirection.setText(model.getWindDirection());
        String windSpeedString = String.valueOf(model.getWindSpeed()) + model.windSpeedUnit();
        windSpeed.setText(windSpeedString);

        relativePressure.setText(model.getPressureString());
        String pressureString = model.getPressure() + "Pa";
        pressurePascals.setText(pressureString);

        String highTempString;
        String lowTempString;
        if (UserPreferences.temperatureUnit == UserPreferences.CELSIUS) {
          highTempString = model.getMaxTemp() + "° C";
          lowTempString = model.getMinTemp() + "° C";
        } else {
          highTempString = model.getMaxTempFahrenheit() + "° F";
          lowTempString = model.getMinTempFahrenheit() + "° F";
        }
        highTemp.setText(highTempString);
        lowTemp.setText(lowTempString);

        Calendar sunriseCalendar = model.sunriseToCalendar();
        Calendar sunsetCalendar = model.sunsetToCalendar();

        if (sunriseCalendar != null) {
          sunriseDate.setText(DateUtils.dateDisplayString(sunriseCalendar));
          sunriseTime.setText(DateUtils.timeDisplayString(sunriseCalendar));
        }

        if (sunsetCalendar != null) {
          sunsetDate.setText(DateUtils.dateDisplayString(sunsetCalendar));
          sunsetTime.setText(DateUtils.timeDisplayString(sunsetCalendar));
        }
      }
    }
  }
}

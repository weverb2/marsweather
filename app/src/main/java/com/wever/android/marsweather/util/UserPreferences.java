package com.wever.android.marsweather.util;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by brandon on 1/16/16.
 */
public class UserPreferences {

  public static final int CELSIUS = 0;
  public static final int FAHRENHEIT = 1;
  public static final String TEMPERATURE_UNIT_KEY = "temperature_unit";

  public static int temperatureUnit = CELSIUS;

  public static SharedPreferences getSharedPreferences(){
    return PreferenceManager.getDefaultSharedPreferences(DaggerApplication.getSharedApplicationContext());
  }

  public static void setTemperatureUnit(int temperatureUnit) {
    if (temperatureUnit <= FAHRENHEIT && temperatureUnit >= CELSIUS){
      UserPreferences.temperatureUnit = temperatureUnit;
      writeTemperatureUnit();
    }
  }

  public static String getTemperatureUnitString(){
    return (temperatureUnit == CELSIUS) ? "C" : "F";
  }

  public static int getTemperatureUnit() {
    readTemperatureUnit();
    return temperatureUnit;
  }

  public static void readTemperatureUnit(){
    temperatureUnit = getSharedPreferences().getInt(TEMPERATURE_UNIT_KEY, CELSIUS);
  }

  public static void writeTemperatureUnit(){
    getSharedPreferences().edit().putInt(TEMPERATURE_UNIT_KEY, temperatureUnit).apply();
  }

}

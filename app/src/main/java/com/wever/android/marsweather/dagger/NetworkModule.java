package com.wever.android.marsweather.dagger;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.wever.android.marsweather.util.RestClient;
import dagger.Module;
import dagger.Provides;

/**
 * Created by brandon on 12/5/15.
 */
@Module
public class NetworkModule {
  @Provides
  public RestClient networkRequest(OkHttpClient httpClient, Bus bus){
    return new RestClient(httpClient, bus);
  }
}

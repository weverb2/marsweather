package com.wever.android.marsweather.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.ui.base.BaseActivity;

/**
 * Created by brandon on 1/16/16.
 */
public class SettingsActivity extends BaseActivity {

  public static Intent newIntent(Context context){
    Intent intent = new Intent(context, SettingsActivity.class);
    return intent;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    if (savedInstanceState == null) {
      SettingsOptionsFragment fragment = SettingsOptionsFragment.newInstance();
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.content_main, fragment, SettingsOptionsFragment.TAG)
          .commit();
    } else {
      // Saved state exists, restore what you need to restore.
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        break;
    }

    return true;
  }

}

package com.wever.android.marsweather.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by brandon on 1/16/16.
 */
public class DateUtils {

  public static Calendar timeStringToCalendar(String timeString) {
    Calendar calendar = null;
    try {
      calendar = ISO8601.toCalendar(timeString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return calendar;
  }

  public static String dateDisplayString(Calendar calendar){
    SimpleDateFormat format = new SimpleDateFormat("M'/'dd'/'yy", Locale.getDefault());
    return format.format(calendar.getTime());
  }

  public static String timeDisplayString(Calendar calendar){
    SimpleDateFormat format = new SimpleDateFormat("h':'m a", Locale.getDefault());
    return format.format(calendar.getTime());
  }
}

package com.wever.android.marsweather.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.ui.base.BaseFragment;
import com.wever.android.marsweather.util.RecyclerItemClickListener;
import com.wever.android.marsweather.util.UserPreferences;

/**
 * Created by brandon on 1/16/16.
 */
public class SettingsOptionsFragment extends BaseFragment
    implements RecyclerItemClickListener.OnItemClickListener {

  public static final String TAG = "SETTINGS_OPTIONS_FRAGMENT";
  private SettingsOptionsAdapter optionsAdapter;

  @Bind(R.id.recyclerview) RecyclerView settingsRecycler;

  public static SettingsOptionsFragment newInstance() {
    Bundle args = new Bundle();
    SettingsOptionsFragment fragment = new SettingsOptionsFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    optionsAdapter = new SettingsOptionsAdapter();
    optionsAdapter.addModel(new Pair<String, String>(getString(R.string.temperature_unit),
        UserPreferences.getTemperatureUnitString()));
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.recycler, container, false);
    ButterKnife.bind(this, root);
    settingsRecycler.setAdapter(optionsAdapter);
    settingsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
    settingsRecycler.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));
    return root;
  }

  @Override public void onItemClick(View view, int position) {
    int viewType = optionsAdapter.getItemViewType(position);
    if (viewType == SettingsOptionsAdapter.TEMPERATURE_VIEW_TYPE) {
      presentTemperatureUnitChoice();
    }
  }

  private void presentTemperatureUnitChoice() {
    new MaterialDialog.Builder(getContext()).title(R.string.temperature_unit)
        .items("Celsius", "Fahrenheit")
        .itemsCallbackSingleChoice(UserPreferences.getTemperatureUnit(),
            new MaterialDialog.ListCallbackSingleChoice() {
              @Override public boolean onSelection(MaterialDialog materialDialog, View view, int i,
                  CharSequence charSequence) {
                UserPreferences.setTemperatureUnit(i);
                optionsAdapter.setModelAtIndex(
                    new Pair<String, String>(getString(R.string.temperature_unit),
                        UserPreferences.getTemperatureUnitString()),
                    SettingsOptionsAdapter.TEMPERATURE_VIEW_TYPE);
                optionsAdapter.notifyItemChanged(SettingsOptionsAdapter.TEMPERATURE_VIEW_TYPE);
                return true;
              }
            })
        .show();
  }
}

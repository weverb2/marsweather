package com.wever.android.marsweather.dagger;

import com.wever.android.marsweather.ui.base.BaseFragment;
import com.wever.android.marsweather.util.RestClient;
import dagger.Component;

/**
 * Created by brandon on 12/5/15.
 */
@PerActivity
@Component(
    dependencies = ApplicationComponent.class,
    modules = NetworkModule.class
)
public interface NetworkComponent {

  void inject(BaseFragment baseFragment);

  RestClient restClient();
}

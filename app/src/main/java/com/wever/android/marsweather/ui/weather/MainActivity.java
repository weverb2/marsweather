package com.wever.android.marsweather.ui.weather;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.wever.android.marsweather.R;
import com.wever.android.marsweather.ui.base.BaseActivity;
import com.wever.android.marsweather.ui.settings.SettingsActivity;

public class MainActivity extends BaseActivity {

  @Bind(R.id.root) CoordinatorLayout coordinatorLayout;
  @Bind(R.id.toolbar) Toolbar toolbar;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    if (savedInstanceState == null){
      MarsReportFragment fragment = MarsReportFragment.newInstance();
      getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment, MarsReportFragment.TAG).commit();
    } else {
      // Saved state exists, restore what you need to restore.
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      startActivity(SettingsActivity.newIntent(this));
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}

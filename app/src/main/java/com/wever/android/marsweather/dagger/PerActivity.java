package com.wever.android.marsweather.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by brandon on 12/4/15.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {}
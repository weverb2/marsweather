package com.wever.android.marsweather.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.squareup.otto.Bus;
import com.wever.android.marsweather.util.DaggerApplication;
import com.wever.android.marsweather.util.RestClient;
import javax.inject.Inject;

/**
 * Created by brandon on 12/18/15.
 */
public class BaseFragment extends Fragment {
  @Inject protected Bus applicationBus;
  @Inject protected RestClient restClient;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DaggerApplication.getNetworkComponent(getContext()).inject(this);
  }

  @Override public void onResume() {
    super.onResume();
    applicationBus.register(this);
  }

  @Override public void onPause() {
    super.onPause();
    applicationBus.unregister(this);
  }
}

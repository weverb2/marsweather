package com.wever.android.marsweather.util;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Bus;
import java.io.IOException;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by brandon on 1/13/16.
 */
public class RestClient {

  private final static String MARS_WEATHER_BASE = "http://marsweather.ingenology.com";

  Bus applicationBus;
  OkHttpClient mClient;
  Retrofit retrofit;
  MarsWeatherReportService marsWeatherReportService;

  public RestClient(OkHttpClient mClient, Bus applicationBus) {
    this.applicationBus = applicationBus;
    this.mClient = mClient;

    mClient.setCache(
        new Cache(DaggerApplication.getSharedApplicationContext().getCacheDir(), 1024 * 1024 * 5));

    mClient.interceptors().add(new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        if (request.header("Cache-Control") == null) {
          HttpUrl url = request.httpUrl()
              .newBuilder()
              .addQueryParameter("Cache-Control", "max-age=120")
              .build();
          request = request.newBuilder().url(url).build();
        }

        return chain.proceed(request);
      }
    });

    Gson gson =
        new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

    retrofit = new Retrofit.Builder().baseUrl(MARS_WEATHER_BASE)
        .client(mClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();

    marsWeatherReportService = retrofit.create(MarsWeatherReportService.class);

  }

  public MarsWeatherReportService getMarsWeatherReportService() {
    return marsWeatherReportService;
  }
}

package com.wever.android.marsweather.dagger;

import android.content.Context;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.wever.android.marsweather.util.DaggerApplication;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Created by brandon on 12/4/15.
 */
@Module
public class ApplicationModule {
  private final DaggerApplication application;

  public ApplicationModule(DaggerApplication application) {
    this.application = application;
  }

  @Provides @Singleton Context provideApplicationContext() {
    return this.application;
  }

  @Provides @Singleton OkHttpClient provideHttpClient(){
    return new OkHttpClient();
  }

  @Provides @Singleton Bus provideApplicationBus(){
    return new Bus();
  }
}

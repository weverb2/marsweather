package com.wever.android.marsweather.dagger;

import android.net.NetworkRequest;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.wever.android.marsweather.ui.base.BaseActivity;
import com.wever.android.marsweather.util.DaggerApplication;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Created by brandon on 12/4/15.
 */
@Singleton @Component(modules = ApplicationModule.class ) public interface ApplicationComponent {

  void inject(BaseActivity baseActivity);

  void inject(DaggerApplication app);

  void inject(NetworkRequest request);

  OkHttpClient okHttpClient();

  Bus bus();
}

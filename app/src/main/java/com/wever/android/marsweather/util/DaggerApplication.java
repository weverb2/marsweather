package com.wever.android.marsweather.util;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import com.wever.android.marsweather.dagger.ApplicationComponent;
import com.wever.android.marsweather.dagger.ApplicationModule;
import com.wever.android.marsweather.dagger.DaggerApplicationComponent;
import com.wever.android.marsweather.dagger.DaggerNetworkComponent;
import com.wever.android.marsweather.dagger.NetworkComponent;
import com.wever.android.marsweather.dagger.NetworkModule;

/**
 * Created by Brandon Wever on 4/23/15.
 * <p/>
 * Copyright E.W. Scripps Company 2015.
 */
public class DaggerApplication extends Application {
  private static DaggerApplication instance;

  private ApplicationComponent applicationComponent;
  private NetworkComponent networkComponent;

  @Override public void onCreate() {
    super.onCreate();
    applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    applicationComponent.inject(this);
    networkComponent = DaggerNetworkComponent.builder().applicationComponent(applicationComponent).networkModule(new NetworkModule()).build();
    instance = this;
  }

  public static ApplicationComponent getComponent(Context context) {
    return ((DaggerApplication) context.getApplicationContext()).applicationComponent;
  }

  public static NetworkComponent getNetworkComponent(Context context){
    return ((DaggerApplication) context.getApplicationContext()).networkComponent;
  }

  public static DaggerApplication getInstance() {
    return instance;
  }

  public static Context getSharedApplicationContext() {
    return instance.getApplicationContext();
  }

  public static float getScreenWidthDp() {
    DisplayMetrics displayMetrics =
        getSharedApplicationContext().getResources().getDisplayMetrics();

    return displayMetrics.widthPixels / displayMetrics.density;
  }

  public static int getScreenWidthPx() {
    DisplayMetrics displayMetrics =
        getSharedApplicationContext().getResources().getDisplayMetrics();

    return displayMetrics.widthPixels;
  }

  public static float getScreenHeightDp() {
    DisplayMetrics displayMetrics =
        getSharedApplicationContext().getResources().getDisplayMetrics();

    return displayMetrics.heightPixels / displayMetrics.density;
  }

  public static int getScreenHeightPx() {
    DisplayMetrics displayMetrics =
        getSharedApplicationContext().getResources().getDisplayMetrics();

    return displayMetrics.heightPixels;
  }

  public static float getScreenDensity() {
    DisplayMetrics displayMetrics =
        getSharedApplicationContext().getResources().getDisplayMetrics();

    return displayMetrics.density;
  }

  public static boolean isConnectedToInternet(){
    ConnectivityManager cm =
        (ConnectivityManager)instance.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    boolean isConnected = activeNetwork != null &&
        activeNetwork.isConnectedOrConnecting();
    return isConnected;
  }
}

package com.wever.android.marsweather.models;

import com.wever.android.marsweather.util.DateUtils;
import java.util.Calendar;

public class WeatherReport {

  private String terrestrialDate;
  private Integer sol;
  private Double ls;
  private Double minTemp;
  private Double minTempFahrenheit;
  private Double maxTemp;
  private Double maxTempFahrenheit;
  private Double pressure;
  private String pressureString;
  private Double absHumidity;
  private Double windSpeed;
  private String windDirection;
  private String atmoOpacity;
  private String season;
  private String sunrise;
  private String sunset;

  /**
   * @return The terrestrialDate
   */
  public String getTerrestrialDate() {
    return terrestrialDate;
  }

  /**
   * @param terrestrialDate The terrestrial_date
   */
  public void setTerrestrialDate(String terrestrialDate) {
    this.terrestrialDate = terrestrialDate;
  }

  /**
   * @return The sol
   */
  public Integer getSol() {
    return sol;
  }

  /**
   * @param sol The sol
   */
  public void setSol(Integer sol) {
    this.sol = sol;
  }

  /**
   * @return The ls
   */
  public Double getLs() {
    return ls;
  }

  /**
   * @param ls The ls
   */
  public void setLs(Double ls) {
    this.ls = ls;
  }

  /**
   * @return The minTemp
   */
  public Double getMinTemp() {
    return minTemp;
  }

  /**
   * @param minTemp The min_temp
   */
  public void setMinTemp(Double minTemp) {
    this.minTemp = minTemp;
  }

  /**
   * @return The minTempFahrenheit
   */
  public Double getMinTempFahrenheit() {
    return minTempFahrenheit;
  }

  /**
   * @param minTempFahrenheit The min_temp_fahrenheit
   */
  public void setMinTempFahrenheit(Double minTempFahrenheit) {
    this.minTempFahrenheit = minTempFahrenheit;
  }

  /**
   * @return The maxTemp
   */
  public Double getMaxTemp() {
    return maxTemp;
  }

  /**
   * @param maxTemp The max_temp
   */
  public void setMaxTemp(Double maxTemp) {
    this.maxTemp = maxTemp;
  }

  /**
   * @return The maxTempFahrenheit
   */
  public Double getMaxTempFahrenheit() {
    return maxTempFahrenheit;
  }

  /**
   * @param maxTempFahrenheit The max_temp_fahrenheit
   */
  public void setMaxTempFahrenheit(Double maxTempFahrenheit) {
    this.maxTempFahrenheit = maxTempFahrenheit;
  }

  /**
   * @return The pressure
   */
  public Double getPressure() {
    return pressure;
  }

  /**
   * @param pressure The pressure
   */
  public void setPressure(Double pressure) {
    this.pressure = pressure;
  }

  /**
   * @return The pressureString
   */
  public String getPressureString() {
    return pressureString;
  }

  /**
   * @param pressureString The pressure_string
   */
  public void setPressureString(String pressureString) {
    this.pressureString = pressureString;
  }

  /**
   * @return The absHumidity
   */
  public Double getAbsHumidity() {
    return absHumidity;
  }

  /**
   * @param absHumidity The abs_humidity
   */
  public void setAbsHumidity(Double absHumidity) {
    this.absHumidity = absHumidity;
  }

  /**
   * @return The windSpeed
   */
  public Double getWindSpeed() {
    if (windSpeed == null){
      return 0.0;
    }
    return windSpeed;
  }

  /**
   * @param windSpeed The wind_speed
   */
  public void setWindSpeed(Double windSpeed) {
    this.windSpeed = windSpeed;
  }

  /**
   * Data source http://cab.inta-csic.es/rems/rems_weather.xml says
   * wind speed is in meters per second, even though it always returns null.
   */
  public String windSpeedUnit(){
    return "m/s";
  }

  /**
   * @return The windDirection
   */
  public String getWindDirection() {
    return windDirection;
  }

  /**
   * @param windDirection The wind_direction
   */
  public void setWindDirection(String windDirection) {
    this.windDirection = windDirection;
  }

  /**
   * @return The atmoOpacity
   */
  public String getAtmoOpacity() {
    return atmoOpacity;
  }

  /**
   * @param atmoOpacity The atmo_opacity
   */
  public void setAtmoOpacity(String atmoOpacity) {
    this.atmoOpacity = atmoOpacity;
  }

  /**
   * @return The season
   */
  public String getSeason() {
    return season;
  }

  /**
   * @param season The season
   */
  public void setSeason(String season) {
    this.season = season;
  }

  /**
   * @return The sunrise
   */
  public String getSunrise() {
    return sunrise;
  }

  /**
   * @param sunrise The sunrise
   */
  public void setSunrise(String sunrise) {
    this.sunrise = sunrise;
  }

  /**
   * @return The sunset
   */
  public String getSunset() {
    return sunset;
  }

  /**
   * @param sunset The sunset
   */
  public void setSunset(String sunset) {
    this.sunset = sunset;
  }

  public Calendar sunriseToCalendar() {
    return DateUtils.timeStringToCalendar(sunrise);
  }

  public Calendar sunsetToCalendar() {
    return DateUtils.timeStringToCalendar(sunset);
  }
}
